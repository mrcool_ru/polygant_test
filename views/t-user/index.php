<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\TUserRole;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuser-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'createdat',
                //'format' => 'date',
                'filter' => yii\jui\DatePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'createdat',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => [
                            'class' => 'form-control'
                        ],
                        'clientOptions' => [
                            'dateFormat' => 'yyyy-MM-dd',
                        ]
                    ]
                )
            ],
            [
                'attribute' => 'user_role',
                'value' => 'roles.name',
                'filter' => Html::activeDropDownList($searchModel, 'user_role', array_merge([''=>''],ArrayHelper::map(TUserRole::find()->asArray()->all(), 'user_role', 'name'))),
            ],
            'name',
            'email:email',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
