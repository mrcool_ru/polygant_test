<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ProfileForm extends Model
{
    public $name;
    
    public function init()
    {
        $this->name = Yii::$app->user->identity->name;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
        ];
    }

    /**
     * Updates user Name
     * @return boolean whether the user is updated in successfully
     */
    public function save()
    {
        if ($this->validate()) {
            Yii::$app->user->identity->name = $this->name;
            return Yii::$app->user->identity->save();
        } else {
            return false;
        }
    }

}
