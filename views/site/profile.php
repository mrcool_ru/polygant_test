<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php $form = ActiveForm::begin([
        'id' => 'profile-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    
    <div class="form-group">
      <label class="col-lg-1 control-label">Email</label>
      <div class="col-lg-3">
        <p class="form-control-static"><?= $user->email ?></p>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-lg-1 control-label">Роль</label>
      <div class="col-lg-3">
        <p class="form-control-static"><?= $user->roles ?></p>
      </div>
    </div>
    
    <?= $form->field($model, 'name') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name' => 'save-button']) ?>
            <?= Html::a('Cменить пароль', Url::toRoute('site/password') ,['class' => 'btn btn-primary', 'name' => 'pass-button']) ?>
            <?= Html::a('Отмена', Url::toRoute('site/index') ,['class' => 'btn btn-default', 'name' => 'cancel-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
