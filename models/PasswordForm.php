<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class PasswordForm extends Model
{
    public $password;
    public $new_password;
    public $password_repeat;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['password','new_password','password_repeat'], 'required'],
            [['password'], 'string', 'max' => 100],
            [['new_password','password_repeat'], 'string', 'min' => 6, 'max' => 100],
            ['password', 'validatePassword'],
            ['new_password', 'compare', 'compareAttribute'=>'password_repeat'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'password_repeat' => 'Повтор пароля',
        ];
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if (!Yii::$app->getSecurity()->validatePassword($this->password, Yii::$app->user->identity->password)) {
                $this->addError($attribute, 'Неправильный пароль.');
            }
        }
    }

    /**
     * Updates user password
     * @return boolean whether the user is updated in successfully
     */
    public function save()
    {
        if ($this->validate()) {
            Yii::$app->user->identity->password = Yii::$app->getSecurity()->generatePasswordHash($this->new_password);
            return Yii::$app->user->identity->save();
        } else {
            return false;
        }
    }

}
