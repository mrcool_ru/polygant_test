<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TUser;

/* @var $this yii\web\View */
/* @var $model app\models\TUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tuser-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    
<?php If ($model->scenario==TUser::SCENARIO_EDIT) :?>
    <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true]) ?>
<?php Else : ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
<?php EndIf; ?>
    <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_role')->dropDownList(ArrayHelper::map(app\models\TUserRole::find()->all(), 'user_role', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
