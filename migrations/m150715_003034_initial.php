<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\TUserRole;

class m150715_003034_initial extends Migration
{
    protected $tableOptions= 'ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8';
    
    public function up()
    {
        // t_user
        $this->createTable('{{%t_user}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . "(100) NOT NULL",
            'email' => Schema::TYPE_STRING . "(100) NOT NULL",
            'password' => Schema::TYPE_STRING . "(100) NOT NULL",
            'createdat' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'user_role' => Schema::TYPE_STRING . "(50) NULL DEFAULT 'user'",
        ], $this->tableOptions);

        // t_user_role
        $this->createTable('{{%t_user_role}}', [
            'user_role' => Schema::TYPE_STRING . "(50) NOT NULL",
            'name' => Schema::TYPE_STRING . "(100) NOT NULL",
            'PRIMARY KEY (user_role)',
        ], $this->tableOptions);

        // fk: t_user
        $this->addForeignKey('fk_t_user_user_role', '{{%t_user}}', 'user_role', '{{%t_user_role}}', 'user_role');
        
        
        //добавляем  роли
        $userRole = new TUserRole();
        $userRole->user_role = 'user';
        $userRole->name = 'Пользователь';
        $userRole->save();
        
        $userRole = new TUserRole();
        $userRole->user_role = 'admin';
        $userRole->name = 'Администратор';
        $userRole->save();
        
        return true;
    }

    public function down()
    {
        echo "m150715_003034_initial cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
