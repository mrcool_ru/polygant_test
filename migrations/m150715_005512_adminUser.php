<?php

use \Yii;
use yii\db\Schema;
use yii\db\Migration;
use app\models\TUser;

class m150715_005512_adminUser extends Migration
{

    public function up()
    {
      $user = new TUser();
      $user->name='Admin';
      $user->email='admin@email.com';
      $user->user_role='admin';
      $user->password='admin';
      $user->save();
      
      echo "Admin created. email:admin@email.com \t pass:admin\n";
    }

    public function down()
    {
        TUser::find()->where(['email' => 'admin@email.com'])->one()->delete();
         echo "Admin(admin@email.com) deleted.\n";
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
