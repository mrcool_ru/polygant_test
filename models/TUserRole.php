<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_user_role".
 *
 * @property string $user_role
 * @property string $name
 */
class TUserRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_role', 'name'], 'required'],
            [['user_role'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_role' => 'User Role',
            'name' => 'Name',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->name;
    }
}
