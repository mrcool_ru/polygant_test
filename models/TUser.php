<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_user".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $createdat
 */
class TUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    const SCENARIO_EDIT = 'edit';
    const SCENARIO_REGISTER = 'register';
    
    public $password_repeat;
    public $new_password;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','email'], 'required'],
            [['user_role','password_repeat'], 'safe'],
            [['name', 'email', 'password'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['password'], 'required', 'on'=> self::SCENARIO_REGISTER],
            [['password'], 'string', 'min' => 6, 'max' => 100, 'on'=> self::SCENARIO_REGISTER],
            ['password', 'compare', 'compareAttribute'=>'password_repeat', 'on'=> self::SCENARIO_REGISTER],
            [['new_password'], 'string', 'min' => 6, 'max' => 100, 'on'=> self::SCENARIO_EDIT],
            ['new_password', 'compare', 'compareAttribute'=>'password_repeat', 'on'=> self::SCENARIO_EDIT],
            ['email', 'filter', 'filter'=>'mb_strtolower'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'password' => 'Пароль',
            'new_password' => 'Новый пароль',
            'password_repeat' => 'Повторите пароль',
            'createdat' => 'Создан',
            'user_role' => 'Роль',
            'roles' => 'Роль',
        ];
    }
    
    /**
     * @inheritdoc
     */  
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_EDIT] = ['name', 'email', 'new_password', 'password_repeat'];
        $scenarios[self::SCENARIO_REGISTER] = ['name', 'email', 'password', 'password_repeat'];
        
        return $scenarios;
    }
    
    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return session_id();
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return session_id() == $authKey;
    }
    
    public function beforeSave($insert)
    {
         if(parent::beforeSave($insert))
         {
            if($this->isNewRecord)
            {
                $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            }

            return true;
         }

        return false;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasOne(TUserRole::className(), ['user_role' => 'user_role']);
    }
}
